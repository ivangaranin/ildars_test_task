#= require jquery
#= require jquery_ujs
#= require jquery.role
#= require role
#= require bootstrap
#= require select2
#= require js-routes
#= require fotorama

$(document).ready ->
  window.IvansTestTask = new IvansTestTask($(document))

class IvansTestTask
  constructor: (@$container) ->
    $('.select_brand').select2({ 
      placeholder: 'Select an phone brand',
      theme: 'bootstrap',
      width: '100%'
    })

    $('.select_brand').on('select2:select', (evt) ->
      $.ajax({
        url: Routes.phones_path({brand: $('.select_brand').val() }),
        type: 'GET',
        dataType: 'script'
      })
    )

    
    $('#search_input').on("change", -> 
      $.ajax({
        url: Routes.search_phones_path({query: $(this).val() }),
        type: 'GET',
        dataType: 'script'
      })
    )