class Gsmarena::Phones
  attr_reader :query, :type, :results, :results_count_text

  def initialize(query)
    @query = query
  end

  def search_by_brand
    @type = 'short'
    @results = []
    response_body = Gsmarena::Connector.get({path: "/#{query}"})
    if response_body.present?
      phones_html = Nokogiri::HTML(response_body)
      parse_phones(phones_html)
      phones_html.xpath('//div[@class="nav-pages"]/a').each do |link|
        handle_pagination(link.attr('href').try(:value))
      end 
    end
    self
  end

  def search_by_query
    @type = 'long'
    @results = []
    response_body = Gsmarena::Connector.get({path: '/results.php3', query: "sQuickSearch=yes&sName=#{query}"})

    if response_body.present?
      return response_body if response_body.is_a?(Gsmarena::Phone)
      phones_html = Nokogiri::HTML(response_body)
      
      parse_phones(phones_html)
      text = phones_html.xpath('//div[@class="st-text"]').first.xpath('p').text
      if text.include?('No phones found!')
        @results_count_text = "No phones found! Try to Google it '<a href='http://google.com/search?ie=UTF-8&q=#{query}' target='_blank'>#{query}</a>'"
      else
        @results_count_text = text
      end
    end
    self
  end

  def parse_phones(html)
    html.xpath('//div[@class="section-body"]/div[@class="makers"]/ul/li').each do |li|
      results << parse_phone(li)
    end
  end

  def parse_phone(li)
    if li.text.present? 
      case type 
      when 'short'
        [li.text, li.xpath('a').attr('href').value] 
      when 'long'
        {
          text: li.text, 
          href: li.xpath('a').attr('href').value,
          title: li.xpath('a').xpath('img').attr('title').value,
          img: li.xpath('a').xpath('img').attr('src').value
        }
      end
    end
  end

  def handle_pagination(href)
    parse_phones(Nokogiri::HTML(Gsmarena::Connector.get({path: "/#{href}"})))
  end 
end
