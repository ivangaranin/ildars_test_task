class Gsmarena::Connector
  def self.get(uri_attributes)
    response = Net::HTTP.get_response(resource_uri(uri_attributes))
    
    if response.code == '200'
      response.body 
    elsif response.code == '302' && response.response['Location'] != nil
      return Gsmarena::Phone.search(response.response['Location'])
    else
      nil
    end
  end

  
  def self.resource_uri(uri_attributes)
    URI::HTTP.build({host: 'www.gsmarena.com'}.merge(uri_attributes))
  end
end