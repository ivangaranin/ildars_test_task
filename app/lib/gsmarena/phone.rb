class Gsmarena::Phone
  attr_reader :attributes, :photos, :name
  attr_writer :name

  def initialize 
    @attributes = []
    @photos = []
    @name = nil
  end

  def self.search(query)
    response_body = Gsmarena::Connector.get({path: "/#{query}"})
    if response_body.present?
      phone_html = Nokogiri::HTML(response_body)
      phone = Gsmarena::Phone.new
      phone.name = phone_html.xpath('//h1[@class="specs-phone-name-title"]').text
      phone.parse_attributes(phone_html.xpath('//div[@id="specs-list"]/table[@cellspacing="0"]'))
      photos_links_html = phone_html.xpath('//div[@class="specs-photo-main"]/a')
      phone.parse_photos(phone_html.xpath('//div[@class="specs-photo-main"]/a').attr('href').value) if photos_links_html.present?
    end
    phone
  end

  def parse_attributes(tables)
    tables.each do |table|
      th = nil
      tds = []
      table.xpath('tr').each do |tr|
        th = tr.xpath('th').text if tr.xpath('th').text.present?
        title = tr.xpath('td[@class="ttl"]').xpath('a').text
        info_block = tr.xpath('td[@class="nfo"]')
        info = if info_block.xpath('a').any?
          info_block.xpath('a').map{|a| a.text}.join(' / ')
        else
          info_block.text
        end
        tds << Hash[title, info]
      end
      attributes << Hash[th, tds]
    end
  end

  def parse_photos(photos_link)
    photos_body = Gsmarena::Connector.get({path: "/#{photos_link}"})
    if photos_body.present?
      photos_html = Nokogiri::HTML(photos_body)
      @photos = photos_html.xpath('//div[@id="pictures-list"]/p[@align="center"]/img').map{|img| img.attr('src')} 
    end
  end
end