class HomeController < ApplicationController
  helper_method :phones_collection, :resource_phone, :phones_search

  def index
  end

  def phones
    respond_to do |format|
      format.js
    end
  end

  def search_phones
    respond_to do |format|
      format.js
    end
  end

  def phone
    respond_to do |format|
      format.js
    end
  end


  protected

  def phones_collection
    return @phones_collection if defined?(@phones_collection)

     @phones_collection =  if phones_search.present? && phones_search.is_a?(Gsmarena::Phone)
      phones_search
    elsif  phones_search.present?
      phones_search.results
    end
  end

  def phones_search
    return @phones_search if defined?(@phones_search)

    @phones_search = if params[:brand].present?
      Gsmarena::Phones.new(params[:brand]).search_by_brand
    else
      Gsmarena::Phones.new(params[:query].strip).search_by_query
    end

  end

  def resource_phone
    @resource_phone ||= Gsmarena::Phone.search(params[:phone_path]) if params[:phone_path].present?
  end

end