require "rails_helper"

RSpec.describe HomeController, :type => :controller do
  describe "GET index" do
    it "should return 200" do
      get :index
      expect(response).to have_http_status(200)
    end

    it "renders the index template" do
      get :index
      expect(response).to render_template("index")
    end
  end

  describe "GET JS phones" do 
    it 'should return 200' do 
      xhr :get, :phones, format: 'js'
      expect(response).to have_http_status(200)
    end

    it 'should return 200 with bad params and brand param' do 
      @params = {random_param: 'red', brand: 'allview-phones-88.php' , format: 'js'}
      xhr :get, :phones, @params 
      expect(response).to have_http_status(200)
    end
  end

  describe "GET JS search_phones" do 
    it 'should return 200' do 
      xhr :get, :search_phones, format: 'js'
      expect(response).to have_http_status(200)
    end

    it 'should return 200 with good brand' do 
      @params = {query: 'at&t-phones-57.php' , format: 'js'}
      xhr :get, :search_phones, @params 
      expect(response).to have_http_status(200)
    end

    it 'should return 200 with good query' do 
      @params = {query: 'phone' , format: 'js'}
      xhr :get, :search_phones, @params 
      expect(response).to have_http_status(200)
    end

    it 'should return 200 with bad query' do 
      @params = {query: 'qweqweqweqweqw' , format: 'js'}
      xhr :get, :search_phones, @params 
      expect(response).to have_http_status(200)
    end

    it 'should return 200 with blank query' do 
      @params = {query: '' , format: 'js'}
      xhr :get, :search_phones, @params 
      expect(response).to have_http_status(200)
    end

    it 'should return 200 with existing one phone query' do 
      @params = {query: 'iphone 4s' , format: 'js'}
      xhr :get, :search_phones, @params 
      expect(response).to have_http_status(200)
    end
  end

  describe "GET JS phone_info" do 
    it 'should return 200' do 
      xhr :get, :phone, @params = {phone_path: 'allview-phones-88.php', format: 'js'}
      expect(response).to have_http_status(200)
    end
  end
end
