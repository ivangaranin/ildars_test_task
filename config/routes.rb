Rails.application.routes.draw do
  root to: 'home#index'
  get 'phones' => 'home#phones'
  get 'search_phones' => 'home#search_phones'
  get 'phone' => 'home#phone'
end
